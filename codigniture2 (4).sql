-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 22, 2018 at 01:25 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `codigniture2`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cat`
--

CREATE TABLE `tbl_cat` (
  `cat_id` int(11) NOT NULL,
  `cat_name` varchar(40) NOT NULL,
  `cat_type` varchar(40) NOT NULL,
  `position` tinyint(1) NOT NULL,
  `cat_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_cat`
--

INSERT INTO `tbl_cat` (`cat_id`, `cat_name`, `cat_type`, `position`, `cat_status`) VALUES
(30, 'Bangladesh', '39', 1, 1),
(31, 'Cricketff', '0', 1, 1),
(32, 'India', '36', 2, 0),
(33, 'Bangladeshg', '30', 0, 0),
(34, 'Pakistan', '31', 1, 0),
(35, 'Football', '36', 0, 1),
(36, 'English', '31', 1, 1),
(37, 'En', '30', 0, 0),
(38, 'football2', '34', 0, 1),
(39, 'Khela', '30', 0, 1),
(40, 'Hospital', '0', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_home`
--

CREATE TABLE `tbl_home` (
  `home_id` int(11) NOT NULL,
  `home_cat_position` int(30) NOT NULL,
  `cat_id` int(30) NOT NULL,
  `max_post` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_home`
--

INSERT INTO `tbl_home` (`home_id`, `home_cat_position`, `cat_id`, `max_post`) VALUES
(30, 1, 31, 5),
(31, 2, 40, 5),
(32, 3, 31, 5);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_menu`
--

CREATE TABLE `tbl_menu` (
  `menu_id` int(11) NOT NULL,
  `cat_id` int(100) NOT NULL,
  `sub_cat_id` int(100) NOT NULL,
  `position` int(4) NOT NULL,
  `menu_status` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_menu`
--

INSERT INTO `tbl_menu` (`menu_id`, `cat_id`, `sub_cat_id`, `position`, `menu_status`) VALUES
(1, 32, 30, 1, 0),
(2, 32, 30, 1, 1),
(3, 30, 0, 2, 1),
(4, 35, 0, 2, 1),
(5, 35, 0, 2, 1),
(6, 30, 32, 1, 0),
(7, 30, 32, 1, 0),
(8, 32, 36, 1, 0),
(9, 32, 36, 1, 1),
(10, 32, 36, 1, 1),
(11, 32, 30, 2, 1),
(12, 32, 30, 2, 1),
(13, 31, 31, 2, 1),
(14, 31, 31, 2, 1),
(15, 0, 0, 1, 0),
(16, 30, 31, 1, 0),
(17, 30, 31, 1, 0),
(18, 30, 31, 1, 0),
(19, 32, 30, 2, 1),
(20, 34, 30, 2, 0),
(21, 32, 30, 2, 1),
(22, 37, 0, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_post`
--

CREATE TABLE `tbl_post` (
  `post_id` int(11) NOT NULL,
  `cat_id` int(30) NOT NULL,
  `title` varchar(50) NOT NULL,
  `short_description` text NOT NULL,
  `long_description` text NOT NULL,
  `tag` varchar(100) NOT NULL,
  `link` varchar(100) NOT NULL,
  `status` int(3) NOT NULL,
  `post_image` varchar(100) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_post`
--

INSERT INTO `tbl_post` (`post_id`, `cat_id`, `title`, `short_description`, `long_description`, `tag`, `link`, `status`, `post_image`, `date`) VALUES
(30, 30, 'munnaalaminghjsdfgsd', 'Various versions have evolved over the years, sometimes by accident, sometimes on purpose you need to be sure there isn’t anything embarrassing hidden in the middle.\r\n', '<p>Various versions have evolved over the years, sometimes by accident, sometimes on purpose you need to be sure there isn&rsquo;t anything embarrassing hidden in the middle.<br />\r\nunchanged.It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages<br />\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&rsquo;s standard dummy<br />\r\ntext ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It hassurvived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<br />\r\nThere are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injectedhumour, or randomised words which don&rsquo;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&rsquo;t anything embarrassing hidden in the middle.</p>\r\n\r\n<p>Various versions have evolved over the years, sometimes by accident, sometimes on purpose you need to be sure there isn&rsquo;t anything embarrassing hidden in the middle.<br />\r\nunchanged.It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages<br />\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&rsquo;s standard dummy<br />\r\ntext ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It hassurvived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<br />\r\nThere are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injectedhumour, or randomised words which don&rsquo;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&rsquo;t anything embarrassing hidden in the middle.</p>\r\n', 'gfhjfghjuuuuuuuuuuuuuuuuuuuuu', 'gfhjfghjuouiouiiiii', 1, 'architecture-3244557_640.jpg', '2018-05-07'),
(31, 31, 'Various versions have evolved over the years, some', 'Various versions have evolved over the years, sometimes by accident, sometimes on purpose you need to be sure there isn’t anything embarrassing hidden in the middle.', '<p>Various versions have evolved over the years, sometimes by accident,</p>\r\n\r\n<p>Various versions have evolved over the years, sometimes by accident,</p>\r\n\r\n<p>Various versions have evolved over the years, sometimes by accident,</p>\r\n\r\n<p>Various versions have evolved over the years, sometimes by accident,</p>\r\n\r\n<p>Various versions have evolved over the years, sometimes by accident,</p>\r\n\r\n<p>Various versions have evolved over the years, sometimes by accident,</p>\r\n', 'asdfasd', 'asdfs ,zsfgsdfgsdfg,alsdkfjalsdkf', 1, 'architecture-3240947_640.jpg', '2018-05-05'),
(34, 0, 'Various versions have evolved over the years, some', 'sdfgsdfgVarious versions have evolved over the years, sometimes by', '<p>Various versions have evolved over the years, sometimes by accident, sometimes on purpose you need to be sure there isn&rsquo;t anything embarrassing&nbsp;</p>\r\n', 'dsfg', '', 0, '', '2018-05-06'),
(35, 32, 'asdfasdf', 'gszdfg', '<p>sdfgsdfgdsfg</p>\r\n', 'aksdf,tag,laalsdf', '', 1, '360X2501.jpg', '2018-05-01'),
(36, 31, 'DUIS URBANITAS EAM IN, TEMPOR CONSEQUAT.', 'Facilisi complectitur eos eu. Est tritani argumentum in, ei suas ignota admodum vim, ipsum choro has ut. Ei vim noluisse luptatum, nominavi mandamus qui ut. Ne usu lucilius mnesarchum, vim ex nisl summo expetenda, in dicta appareat usu. Ea cum altera fuisset adipisci, in sed eius tacimates, eu duo magna numquam placerat.', '<p>Facilisi complectitur eos eu. Est tritani argumentum in, ei suas ignota admodum vim, ipsum choro has ut. Ei vim noluisse luptatum, nominavi mandamus qui ut. Ne usu lucilius mnesarchum, vim ex nisl summo expetenda, in dicta appareat usu. Ea cum altera fuisset adipisci, in sed eius tacimates, eu duo magna numquam placerat.</p>\r\n\r\n<p>Sea at dolorum nominavi adipiscing, ei eam mundi legimus, sit deleniti definiebas et. Pri dicit latine reformidans ne, offendit rationibus incorrupte an qui, eum populo molestie tacimates te. Nec ea facer vituperatoribus, cu pro feugiat minimum platonem. Elit accusam ei per. Duis illum est ut.</p>\r\n\r\n<p>Ex eos esse sale eligendi. Eos ut exerci audire nostrum, at pro dolores tacimates voluptaria. Facete disputando at quo, omittantur philosophia id qui. Ad labore facete suscipiantur sed. Cu iisque sanctus inciderint has, per quodsi liberavisse ea.</p>\r\n\r\n<p>Sit falli nonumes atomorum ex, ipsum populo iisque eum at. Sumo solet omnium eum ad, quis omnium ut ius, volumus splendide sed ad. Mea vide dicta ne, appareat patrioque has an. Wisi sale delectus eum eu, corpora salutatus no sit. Sale interesset eu per.</p>\r\n\r\n<p>Sit falli nonumes atomorum ex, ipsum populo iisque eum at</p>\r\n\r\n<p>Facilisi complectitur eos eu. Est tritani argumentum in, ei suas ignota admodum vim, ipsum choro has ut. Ei vim noluisse luptatum, nominavi mandamus qui ut. Ne usu lucilius mnesarchum, vim ex nisl summo expetenda, in dicta appareat usu. Ea cum altera fuisset adipisci, in sed eius tacimates, eu duo magna numquam placerat.</p>\r\n\r\n<p>Sea at dolorum nominavi adipiscing, ei eam mundi legimus, sit deleniti definiebas et. Pri dicit latine reformidans ne, offendit rationibus incorrupte an qui, eum populo molestie tacimates te. Nec ea facer vituperatoribus, cu pro feugiat minimum platonem. Elit accusam ei per. Duis illum est ut.</p>\r\n\r\n<p>Ex eos esse sale eligendi. Eos ut exerci audire nostrum, at pro dolores tacimates voluptaria. Facete disputando at quo, omittantur philosophia id qui. Ad labore facete suscipiantur sed. Cu iisque sanctus inciderint has, per quodsi liberavisse ea.</p>\r\n\r\n<p>Sit falli nonumes atomorum ex, ipsum populo iisque eum at. Sumo solet omnium eum ad, quis omnium ut ius, volumus splendide sed ad. Mea vide dicta ne, appareat patrioque has an. Wisi sale delectus eum eu, corpora salutatus no sit. Sale interesset eu per.</p>\r\n', 'asdfasd,salkdf,asldkf,', '', 1, 'img-lg-3.jpg', '2018-05-03'),
(37, 31, 'munna', 'Various versions have evolved over the years, sometimes by accident, sometimes on purpose you need to be sure there isn’t anything embarrassin', '<p>Various versions have evolved over the years, sometimes by accident, sometimes on purpose you need to be sure there isn&rsquo;t anything embarrassin</p>\r\n\r\n<p>Various versions have evolved over the years, sometimes by accident, sometimes on purpose you need to be sure there isn&rsquo;t anything embarrassinVarious versions have evolved over the years, sometimes by accident, sometimes on purpose you need to be sure there isn&rsquo;t anything embarrassinVarious versions have evolved over the years, sometimes by accident, sometimes on purpose you need to be sure there isn&rsquo;t anything embarrassin</p>\r\n', 'sdfgsdf', 'gsdfg', 1, 'arrow-2889040_640.jpg', '2018-05-08'),
(38, 40, 'DUIS URBANITAS EAM IN, TEMPOR CONSEQUAT.', 'DUIS URBANITAS EAM IN, TEMPOR CONSEQUAT.DUIS URBANITAS EAM IN, TEMPOR CONSEQUAT.DUIS URBANITAS EAM IN, TEMPOR CONSEQUAT.', '<p>Just add the&#39;order_by&#39; clause to your code and modify it to look just like the one below.Just add the&#39;order_by&#39; clause to your code and modify it to look just like the one below.Just add the&#39;order_by&#39; clause to your code and modify it to look just like the one below.Just add the&#39;order_by&#39; clause to your code and modify it to look just like the one below.Just add the&#39;order_by&#39; clause to your code and modify it to look just like the one below.Just add the&#39;order_by&#39; clause to your code and modify it to look just like the one below.Just add the&#39;order_by&#39; clause to your code and modify it to look just like the one below.Just add the&#39;order_by&#39; clause to your code and modify it to look just like the one below.</p>\r\n', '', '', 1, 'profile-bg-4.jpg', '2018-05-02'),
(40, 40, 'asdf', 'asfasdfasdf', '<p>asdfasdfa</p>\r\n', 'sdfas', 'dfasdfasdf', 1, 'author-header1.jpg', '2018-05-09'),
(41, 0, 'DUIS URBANITAS EAM IN, TEMPOR CONSEQUAT.', 'Jul 22, 2016 - how to get last 2 days record in mysql using php, php mysql select last 24 hours, mysql get last 2 days records, mysql get last 2 days records laravel, mysql last ... We may sometimes require to get few days ago records from table like last 2 days, last 3 days, last 10 days or last 15 days etc. ... Featured Post ...', '<p>Jul 22, 2016 -&nbsp;how to get&nbsp;<em>last</em>&nbsp;2&nbsp;<em>days</em>&nbsp;record in mysql using php, php mysql select&nbsp;<em>last</em>&nbsp;24 hours, mysql get&nbsp;<em>last</em>&nbsp;2&nbsp;<em>days</em>&nbsp;records, mysql get&nbsp;<em>last</em>&nbsp;2&nbsp;<em>days</em>&nbsp;records laravel, mysql&nbsp;<em>last</em>&nbsp;... We may sometimes require to get few&nbsp;<em>days</em>&nbsp;ago records from table like&nbsp;<em>last</em>&nbsp;2&nbsp;<em>days</em>,&nbsp;<em>last 3 days</em>,&nbsp;<em>last</em>&nbsp;10&nbsp;<em>days</em>&nbsp;or&nbsp;<em>last</em>&nbsp;15<em>days</em>&nbsp;etc. ... Featured&nbsp;<em>Post</em>&nbsp;...Jul 22, 2016 -&nbsp;how to get&nbsp;<em>last</em>&nbsp;2&nbsp;<em>days</em>&nbsp;record in mysql using php, php mysql select&nbsp;<em>last</em>&nbsp;24 hours, mysql get&nbsp;<em>last</em>&nbsp;2&nbsp;<em>days</em>&nbsp;records, mysql get&nbsp;<em>last</em>&nbsp;2&nbsp;<em>days</em>&nbsp;records laravel, mysql&nbsp;<em>last</em>&nbsp;... We may sometimes require to get few&nbsp;<em>days</em>&nbsp;ago records from table like&nbsp;<em>last</em>&nbsp;2&nbsp;<em>days</em>,&nbsp;<em>last 3 days</em>,&nbsp;<em>last</em>&nbsp;10&nbsp;<em>days</em>&nbsp;or&nbsp;<em>last</em>&nbsp;15<em>days</em>&nbsp;etc. ... Featured&nbsp;<em>Post</em>&nbsp;...Jul 22, 2016 -&nbsp;how to get&nbsp;<em>last</em>&nbsp;2&nbsp;<em>days</em>&nbsp;record in mysql using php, php mysql select&nbsp;<em>last</em>&nbsp;24 hours, mysql get&nbsp;<em>last</em>&nbsp;2&nbsp;<em>days</em>&nbsp;records, mysql get&nbsp;<em>last</em>&nbsp;2&nbsp;<em>days</em>&nbsp;records laravel, mysql&nbsp;<em>last</em>&nbsp;... We may sometimes require to get few&nbsp;<em>days</em>&nbsp;ago records from table like&nbsp;<em>last</em>&nbsp;2&nbsp;<em>days</em>,&nbsp;<em>last 3 days</em>,&nbsp;<em>last</em>&nbsp;10&nbsp;<em>days</em>&nbsp;or&nbsp;<em>last</em>&nbsp;15<em>days</em>&nbsp;etc. ... Featured&nbsp;<em>Post</em>&nbsp;...</p>\r\n', '', '', 0, 'layoutstyle_4.png', '2018-05-10'),
(42, 31, 'DUIS URBANITAS EAM IN, TEMPOR CONSEQUAT.', 'Jul 22, 2016 - how to get last 2 days record in mysql using php, php mysql select last 24 hours, mysql get last 2 days records, mysql get last 2 days records laravel, mysql last ... We may sometimes require to get few days ago records from table like last 2 days, last 3 days, last 10 days or last 15 days etc. ... Featured Post ...', '<p>Jul 22, 2016 -&nbsp;how to get&nbsp;<em>last</em>&nbsp;2&nbsp;<em>days</em>&nbsp;record in mysql using php, php mysql select&nbsp;<em>last</em>&nbsp;24 hours, mysql get&nbsp;<em>last</em>&nbsp;2&nbsp;<em>days</em>&nbsp;records, mysql get&nbsp;<em>last</em>&nbsp;2&nbsp;<em>days</em>&nbsp;records laravel, mysql&nbsp;<em>last</em>&nbsp;... We may sometimes require to get few&nbsp;<em>days</em>&nbsp;ago records from table like&nbsp;<em>last</em>&nbsp;2&nbsp;<em>days</em>,&nbsp;<em>last 3 days</em>,&nbsp;<em>last</em>&nbsp;10&nbsp;<em>days</em>&nbsp;or&nbsp;<em>last</em>&nbsp;15<em>days</em>&nbsp;etc. ... Featured&nbsp;<em>Post</em>&nbsp;...</p>\r\n', '', '', 1, 'header.png', '2018-05-10');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `user_id` int(200) NOT NULL,
  `username` varchar(40) NOT NULL,
  `email` varchar(25) NOT NULL,
  `password` varchar(40) NOT NULL,
  `type` int(30) NOT NULL,
  `status` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `username`, `email`, `password`, `type`, `status`) VALUES
(11, 'alamin', 'alamin@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 1, 1),
(13, 'user', 'user@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_types`
--

CREATE TABLE `tbl_user_types` (
  `user_type_id` int(11) NOT NULL,
  `user_type` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_user_types`
--

INSERT INTO `tbl_user_types` (`user_type_id`, `user_type`) VALUES
(1, 'admin'),
(2, 'Author'),
(3, 'Modarator'),
(4, 'Oparetor');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_cat`
--
ALTER TABLE `tbl_cat`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `tbl_home`
--
ALTER TABLE `tbl_home`
  ADD PRIMARY KEY (`home_id`);

--
-- Indexes for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `tbl_post`
--
ALTER TABLE `tbl_post`
  ADD PRIMARY KEY (`post_id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `tbl_user_types`
--
ALTER TABLE `tbl_user_types`
  ADD PRIMARY KEY (`user_type_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_cat`
--
ALTER TABLE `tbl_cat`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `tbl_home`
--
ALTER TABLE `tbl_home`
  MODIFY `home_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `tbl_post`
--
ALTER TABLE `tbl_post`
  MODIFY `post_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `user_id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tbl_user_types`
--
ALTER TABLE `tbl_user_types`
  MODIFY `user_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
