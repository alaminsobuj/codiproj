<?php
/**
* 
*/
class Admin_login_model extends CI_Model
{
	public function check_validation($username,$password){
	  $username=$this->input->post('username',true);
 	  $password=md5($this->input->post('password',true));
 	  $this->db->select('*');
 	  $this->db->from('tbl_user');
 	  $this->db->where('username',$username);
 	  $this->db->where('password',$password);
 	  $query_result = $this->db->get();
 	  $result = $query_result->row();
    return $result;
	}

// view profiles 
	public function view_user_info(){
 	  $this->db->select('*');
 	  $this->db->from('tbl_user');
 	  $query_result = $this->db->get();
 	  $result = $query_result->row(); 
    return $result;
        
	}

// view all user
	public function view_all_user(){
 	  $this->db->select('*');
 	  $this->db->from('tbl_user');
 	  $query_result = $this->db->get();
 	  $all_user = $query_result->result(); 
    return $all_user;    
	}
function update_user_info($user_id){
    $this->db->select('*');
 	  $this->db->from('tbl_user');
 	  $this->db->where('user_id',$user_id);
 	  $query_result = $this->db->get();
 	  $result = $query_result->row();       
    return $result;
}

public function update_user_content($data){
    $this->db->where('user_id',$data['user_id']);
    $this->db->update('tbl_user',$data);
     
}

function insert_cat($data){
  $this->db->insert('tbl_cat',$data);
}
function view_cat_settings(){

  //      $this->db->select('*');
 	//   $this->db->from('tbl_cat');
 	
 	// $query_result = $this->db->get();

 	//   $all_user = $query_result->result();
       
  //      return $all_user;

    $this->db->select('t1.cat_id,t1.cat_name,t1.cat_status, t2.cat_id as sid,t2.cat_type,t2.cat_name as sub_menu_name')
     ->from('tbl_cat as t1')
     ->join('tbl_cat as t2', 't1.cat_id = t2.cat_type', 'LEFT');
    $query_result = $this->db->get();
    $all_user = $query_result->result();
    return $all_user;

}



function cat_view(){
  $query = $this->db->query("SELECT * FROM tbl_cat Where cat_type='0'");
  return $query->result();
}

function cat_edit($cat_id){
    $this->db->select('*');
    $this->db->from('tbl_cat');
    $this->db->where('cat_id',$cat_id);
    $query_result = $this->db->get();
    $result = $query_result->row();
    return $result;
}


public function update_cat($data){
    $this->db->where('cat_id',$data['cat_id']);
    $this->db->update('tbl_cat',$data);

       
}

function show_post_category(){
    $this->db->select('*');
    $this->db->from('tbl_cat');
    $query_result = $this->db->get();
    $all_category = $query_result->result();
    return $all_category;  
}
function show_category_front(){
    $this->db->select('*');
    $this->db->where('position',1);
    $this->db->from('tbl_cat');
    $query_result = $this->db->get();
    $all_category = $query_result->result();
    return $all_category;  
}

function subcategory_front(){
    $this->db->select('*');
    $this->db->from('tbl_cat');
    $query_result = $this->db->get();
    $all_category = $query_result->result();
    return $all_category;  
}


// insert db post 
function insert_post($data){
  $this->db->insert('tbl_post',$data);
}
function show_post_list($data){
  $this->db->select("tbl_post.*,tbl_cat.*");
  $this->db->from('tbl_post');
  $this->db->join('tbl_cat', 'tbl_cat.cat_id = tbl_post.cat_id');
  $this->db->where('date BETWEEN DATE_SUB(NOW(), INTERVAL 3 DAY) AND NOW()');
  $this->db->order_by("tbl_post.post_id", "DESC");
  $query = $this->db->get();
  return $query->result();
}

function category_page($cat_id,$limit,$start){


  $this->db->select("tbl_post.*,tbl_cat.*");
  $this->db->from('tbl_post');
  $this->db->join('tbl_cat', 'tbl_cat.cat_id = tbl_post.cat_id');
  $this->db->where('tbl_post.cat_id',$cat_id);
  $this->db->limit($limit,$start);
  $query = $this->db->get();
  return $query->result();
}
function category_details_page($post_id){
  $this->db->select("tbl_post.*,tbl_cat.*");
  $this->db->from('tbl_post');
  $this->db->join('tbl_cat', 'tbl_cat.cat_id = tbl_post.cat_id');
  $this->db->where('post_id',$post_id);
  $query_result = $this->db->get();
  $result = $query_result->row(); 
  return $result;
}


function post_edit($post_id){
  $this->db->select('*');
  $this->db->from('tbl_post');
  $this->db->where('post_id',$post_id);
  $query_result = $this->db->get();
  $result = $query_result->row(); 
  return $result;
}

public function update_post($data){
    $this->db->where('post_id',$data['post_id']);
    $this->db->update('tbl_post',$data);     
}

// add Menu

function insert_menu_db($data){
  $this->db->insert('tbl_menu',$data);
}
function view_menu_list(){

   $this->db->select("tbl_menu.menu_id,tbl_menu.cat_id,tbl_menu.sub_cat_id,tbl_menu.position,tbl_menu.menu_status,tbl_cat.cat_id,tbl_cat.cat_name,tbl_cat.cat_type");
  $this->db->from('tbl_menu');
  $this->db->join('tbl_cat', 'tbl_cat.cat_id = tbl_menu.cat_id');
    // $this->db->select('*');
    // $this->db->from('tbl_menu');
    $query_result = $this->db->get();
    $all_category = $query_result->result();
    return $all_category; 


}

function menu_edit($menu_id){
 $this->db->select('*');
  $this->db->from('tbl_menu');
  $this->db->where('menu_id',$menu_id);
  $query_result = $this->db->get();
  $result = $query_result->row(); 
  return $result;
}
public function update_menu($data){
    $this->db->where('menu_id',$data['menu_id']);
    $this->db->update('tbl_menu',$data);     
}

function Post_pagination($cat_id){

 // $this->db->where('cat_id',$cat_id);

 // return $this->db->count_all_results('tbl_post', FALSE);

  $this->db->select("count(tbl_post.post_id) as total");

  $this->db->from('tbl_post');
  $this->db->join('tbl_cat', 'tbl_cat.cat_id = tbl_post.cat_id');
  $this->db->where('tbl_post.cat_id',$cat_id);
  $query_result = $this->db->get();
  $result = $query_result->row(); 
  return $result;
}



  public function user_rol(){
    $this->db->select('*');
    $this->db->from('tbl_user_types');
    $query_result = $this->db->get();
    $all_user = $query_result->result(); 
    return $all_user;    
  }

function insert_user($data,$email){
  $query = $this->db->get_where('tbl_user', array('email' => $email));
if ($query->num_rows() > 0) {
  // Show error message as email already exists in the database
} else {

  $this->db->insert('tbl_user',$data);
  }


}

function view_user(){
   // $this->db->select('*');
   //  $this->db->from('tbl_user');
   $this->db->select("tbl_user.*, tbl_user_types.*");
  $this->db->from('tbl_user');
  $this->db->join(' tbl_user_types', '  tbl_user_types.user_type_id = tbl_user.type');
    $query_result = $this->db->get();
    $result = $query_result->result();
    return $result;
}

function edit_main_user($user_id){
   $this->db->select('*');
  $this->db->from('tbl_user');
  $this->db->where('user_id',$user_id);
  $query_result = $this->db->get();
  $result = $query_result->row(); 
  return $result;
}
public function update_main_user($data){
   $this->db->where('user_id',$data['user_id']);
   $this->db->update('tbl_user',$data);



}

function insert_home_cat_position($data){
   $this->db->insert('tbl_home',$data);
}


function show_home_page(){
    //   $this->db->select('*');
    // $this->db->from('tbl_home');
    // $query_result = $this->db->get();
    // $result = $query_result->row(); 
    // return $result;

       $this->db->select("tbl_home.*,tbl_cat.*");
       $this->db->from('tbl_home');
   $this->db->join('tbl_cat', ' tbl_cat.cat_id= tbl_home.cat_id');
    $query_result = $this->db->get();
    $result = $query_result->result();
    return $result;

}



// fornt page position wise data show start





function show_home_page_fornt(){

   $this->db->select("tbl_home.*,tbl_cat.*");
   $this->db->from('tbl_home');

   $this->db->join('tbl_cat', 'tbl_home.cat_id= tbl_cat.cat_id');
   $this->db->where('tbl_home.home_id',30);
  
      $query_result = $this->db->get();
      $result = $query_result->result();
      return $result;


}
function show_home_page_fornt2(){

   $this->db->select("tbl_home.*,tbl_cat.*");
   $this->db->from('tbl_home');

   $this->db->join('tbl_cat', 'tbl_home.cat_id= tbl_cat.cat_id');
   $this->db->where('tbl_home.home_id',31);
  
      $query_result = $this->db->get();
      $result = $query_result->result();
      return $result;


}






public function fornt_home_post1(){
$this->db->select('*');    
$this->db->from('tbl_post');
$this->db->order_by('post_id', "desc");
 $this->db->limit(2);
  $query_result = $this->db->get();
  $result = $query_result->result();
    return $result;
}



public function page_fornt(){
$this->db->select('*');    
$this->db->from('tbl_post');
$this->db->order_by('post_id', "desc");
$this->db->limit(12);
  $query_result = $this->db->get();
  $result = $query_result->result();
    return $result;
}





// fornt page position wise data show End



}