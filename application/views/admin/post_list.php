<?php 

?>


            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Tables</h1>
                        <div class="alert alert-success">
                            <?php
                       
                            
                    
                           ?>
                        </div>
         
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                DataTables Advanced Tables
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="dataTable_wrapper">
                                    <table class="table table-striped table-bordered table-hover" id="dataTables-example2">
                                        <thead>
                                            <tr>
                                                <th>SL</th>
                                                <th>Category</th>
                                                <th>Title</th>
                                          
                                                <th>Tag</th>
                                                <th>Link</th>
                                                <th>Activation</th>
                                                <th>Image</th>
                                                <th>Action</th>
                                                
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                             $s=1;
                                            foreach ($show_post_list as   $key=>$value) {
                                               var_dump($value);
                                             ?>
                                            <tr >
                                                <td><?php echo $s++;?></td>
                                                <td><?php echo $value->cat_name;?></td>
                                                <td><?php echo $value->title?></td>
                                     
                                            <td class="center"><?php echo $value->tag;?></td> 
                                            <td class="center"><?php echo $value->link?></td> 
                                            <td class="center"><?php 
                                            
                                                     
                                                    if ($value->status == 1) {
                                                ?>
                                                <a href="<?php echo base_url();?>Admin/pending/<?php echo $value->post_id ?>">
                                                    <i class="fa fa-times" aria-hidden="true"></i>Pending</a>
                                           
                                                <?php        
                                                    }else{
                                                ?>
                                                 <a href="<?php echo base_url();?>Admin/published/<?php echo $value->post_id ?>">
                                                    <i class="fa fa-check" aria-hidden="true"></i>Published</a>
                                                </td>
                                                <?php
                                                    }
                                          
                                                  

                                            ?></td> 
                                            <td class="center">
                                                <?php
                                               
                                                ?>
                                                
                                                   <img src="<?php echo base_url();?>assets/uploads/post_image/<?php echo $value->post_image;?>?>" height="100px" width="100px">
                                            </td> 
                                          
                                                <td class="center">
                                                <a href="<?php echo base_url();?>Admin/edit_post/<?php echo $value->post_id ?>">
                                                <button class="btn btn-info " type="button"><i class="fa fa-paste"></i> Edit</button></a>

                                                <a href="<?php echo base_url();?>Admin/delete_post/<?php echo $value->post_id ?>">
                                                <button class="btn btn-info " type="button"><i class="fa fa-paste"></i> Delete</button></a></td>                                                 
                                            </tr>
                                          <?php  }?>
                                      
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                     
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
           
            </div>
            <!-- /#page-wrapper -->
