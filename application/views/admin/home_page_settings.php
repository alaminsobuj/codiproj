            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                   <section class="content"> 

                        <div class="col-md-8">
                          <h1 class="page-header">Home Page Settings</h1>

                          <!-- general form elements -->
                          <div class="box box-primary">
                          
                            <!-- /.box-header -->
                          <?php 
                           $message=$this->session->userdata('success');
                           if(isset($message)){
                           ?>
                           <div class="alert alert-success alert-dismissible fade in">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <?php echo $message;?>
                           </div>
                          <?php 
                           $this->session->unset_userdata('success');}
                           ?>

                            <!-- form start -->
                            <form action="<?php echo base_url();?>Admin/home_category_position" enctype="multipart/form-data" method="post">

                      
                                <div class="row-fluid">
                                    <div class="col-sm-3">
                                        <input type="number" name="home_cat_position" class="form-control" required="1" style="width:100%;" placeholder="Position No" tabindex="0">
                                    </div>
                                    <div class="col-sm-3">
                                        <select name="category_name" class="form-control" required="1" style="width: 100%;" tabindex="1">
                                            <option value="">Category Name</option>
                                            <?php
                                              foreach ($show_category as $value) {
                                                
                                             ?>
                                            <option value="<?php echo $value->cat_id;?>"><?php echo $value->cat_name ?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="number" name="max_news" placeholder="Max News" min="0" max="12" class="form-control col-sm-3" required="1" tabindex="2">
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="submit" name="save" value="Add Position" class="btn btn-primary" tabindex="3">
                                    </div>

                                </div>              
                            </form>



                        </div>
                          <!-- /.box -->
                      </div>
                  <div class="row">
            <div class="col-sm-10">
       
                <form action="<?php echo base_url();?>Admin/update_home_page" method="post" accept-charset="utf-8">
                  

                        <input type="submit" value="Update" class="btn btn-primary btn-lg pull-right" style="margin-bottom: 5px;"><br>
                        <table class="table table-bordered table-hover">
                            <tbody><tr>
                                <th>Position No</th>
                                <th>Category Name</th>
                                <th>Max News</th>
                                <th>Status</th>
                            </tr>
                                 <?php 
                                    foreach ($show_home_page_data as $key => $values) {
                                  ?>
                                    <tr>
                                    <td>
                                    <input type="hidden" name="home_id[]" value="<?php echo $values->home_id;?>">
                                    <input type="hidden" value="<?php echo $values->home_cat_position;?>" name="home_cat_position[]"><?php echo $values->home_cat_position;?></td>
                                    <td style="width: 50%;">
                                         
                                        <select name="category_name[]" class="form-control" required="1" style="width: 100%;">

                                            <option value="">Category Name</option>
                                           <?php
                                        
                                              foreach ($show_category as $key => $show_categorys) {
                                            ?>
                                     
                                            <option value="<?php echo $show_categorys->cat_id; ?>" <?php if($values->cat_id==$show_categorys->cat_id){ echo "selected";} ?>><?php echo $show_categorys->cat_name;?></option>
                                            <?php }?>
                                                                                    
                                        </select>
                                    </td>
                                    <td>
                                        <input type="number" name="max_news[]" placeholder="Max News" min="0" max="12" class="form-control " style="width: 50%;" required="1" value="<?php echo $values->max_post;?>">
                                    </td>
                                      <td>
                                            jghj
                                      </td>

                                  </tr>
                                  <?php }?>

                                 </tbody>
                             </table>
                       </form>                               
                    </div>
                 </div>   
                        
                    </section>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->