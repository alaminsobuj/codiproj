<?php
?>


            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                   <section class="content"> 

                        <div class="col-md-8">
                          <h1 class="page-header">Add User</h1>
                          <!-- general form elements -->
                          <div class="box box-primary">
                            <div class="box-header with-border">
                              <h3 class="box-title">Register <i class="glyphicon glyphicon-user"></i></h3>
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            <form action="<?php echo base_url();?>Admin/save_user" enctype="multipart/form-data" method="post">
                     
                             <div class="box-body">
                            <div class="form-group">        
                                <div class="row single_field">
                                    <div class="col-sm-4"><label>Full Name</label></div>
                                    <div class="col-sm-8">
                                        <input type="text" name="username" class="form-control" required="">
                                    </div>
                                </div>
                                <div class="row single_field">
                                    <div class="col-sm-4"><label>E-mail </label></div>
                                    <div class="col-sm-8">
                                        <input type="email" name="email" class="form-control" required="">
                                    </div>
                                </div>
                                <div class="row single_field">
                                    <div class="col-sm-4"><label>Password </label></div>
                                    <div class="col-sm-8">
                                        <input type="password" name="password" class="form-control" required="">
                                    </div>
                                </div>                
                        


                                <div class="row single_field">
                                    <div class="col-sm-4"><label>Access category </label></div>
                                    <div class="col-sm-8">
                                        <select name="type" class="form-control">
                                            <option value="">-Access category-</option>
                                            <?php
                                           foreach ($user_rol as $key => $value) {
                                      
                                            ?>
                                            <option value="<?php echo $value->user_type_id ?>"><?php echo $value->user_type;?></option>
                                           <?php }?>

                                        </select>
                                    </div>
                                </div>
                                <input type="hidden" name="status" value="0"/>
                                <div class="row single_field">
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-8">
                                        <input type="submit" name="save" value="Create" class="btn btn-primary">
                                    </div>
                                </div> 
                            </div>  
                            </div>                    
                        </form>
                        </div>
                          <!-- /.box -->
                      </div>

                        
                    </section>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->

        




