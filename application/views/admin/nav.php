   <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="navbar-header">

                </div>

                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><a href="<?php echo base_url();?>"><i class="fa fa-home fa-fw"></i> Website</a></li>
                </ul>

                <ul class="nav navbar-right navbar-top-links">
                    <li class="dropdown navbar-inverse">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-bell fa-fw"></i> <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu dropdown-alerts">
                            <li>
                                <a href="#">
                                    <div>
                                        <i class="fa fa-comment fa-fw"></i> New Comment
                                        <span class="pull-right text-muted small">4 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div>
                                        <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                        <span class="pull-right text-muted small">12 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div>
                                        <i class="fa fa-envelope fa-fw"></i> Message Sent
                                        <span class="pull-right text-muted small">4 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div>
                                        <i class="fa fa-tasks fa-fw"></i> New Task
                                        <span class="pull-right text-muted small">4 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div>
                                        <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                        <span class="pull-right text-muted small">4 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a class="text-center" href="#">
                                    <strong>See All Alerts</strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i> 
                              <?php
                            $username=$this->session->userdata('username');
                            if(isset($username)){
                              echo $username;
                              //$this->session->unset_userdata('username');
                            } 
                            ?>
                             <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="<?php echo base_url();?>Admin/user_profiles"><i class="fa fa-user fa-fw"></i> User Profile</a>
                            </li>
                            <li><a href="<?php echo base_url();?>Admin/user_settings"><i class="fa fa-gear fa-fw"></i> Settings</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="<?php echo base_url();?>Admin/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <!-- /.navbar-top-links -->

                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li class="sidebar-search">
                                <div class="input-group custom-search-form">
                                    <input type="text" class="form-control" placeholder="Search...">
                                    <span class="input-group-btn">
                                        <button class="btn btn-primary" type="button">
                                            <i class="fa fa-search"></i>
                                        </button>
                                </span>
                                </div>
                                <!-- /input-group -->
                            </li>
                            <li>
                                <a href="<?php echo base_url();?>Admin/admn_dashboard" class="active"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                            </li>
                            
                          
                          
                            <li>
                             
                                <a href="#"><i class="fa fa-wrench fa-fw"></i>Post<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">

                                   
                                    <li>
                                        <a href="<?php echo base_url();?>Admin/add_posts">Add Post</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url();?>Admin/post_list">Posts List</a>
                                    </li>

                                    
                                </ul>
                     
                                <!-- /.nav-second-level -->
                            </li>  
                            <li>
                             
                                <a href="#"><i class="fa fa-wrench fa-fw"></i>Menu<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="<?php echo base_url();?>Admin/add_menu">Add Menu</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url();?>Admin/menu_list_page">Menu Settings</a>
                                    </li>
                                    
                                </ul>
                     
                                <!-- /.nav-second-level -->
                            </li>  
                            <li>
                             
                                <a href="#"><i class="fa fa-wrench fa-fw"></i>Category<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="<?php echo base_url();?>Admin/add_category">Add Category</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url();?>Admin/cat_settings">Category Settings</a>
                                    </li>
                                    
                                </ul>
                     
                                <!-- /.nav-second-level -->
                            </li> 

                            <li>
                             
                                <a href="#"><i class="fa fa-wrench fa-fw"></i>Settings<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="<?php echo base_url();?>Admin/home_view_settings">Home Page</a>
                                    </li>
                                    <li>
                                        <a href="#">Website Logo</a>
                                    </li>
                                    <li>
                                        <a href="#">Website  Faveicon</a>
                                    </li>
                                    <li>
                                        <a href="#">Website  Title</a>
                                    </li>
                                    
                                    
                                </ul>
                     
                                <!-- /.nav-second-level -->
                            </li>
                           <li>
                             
                                <a href="#"><i class="fa fa-wrench fa-fw"></i>User<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="<?php echo base_url();?>Admin/add_user">Add User</a>
                                    </li>   

                                    <li>
                                        <a href="<?php echo base_url();?>Admin/view_user_list">User List</a>
                                    </li>
                                 

                                    
                                </ul>
                     
                                <!-- /.nav-second-level -->
                            </li> 
                       
                        </ul>
                    </div>
                </div>
            </nav>