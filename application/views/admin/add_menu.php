            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">Add Posts</h1>
                                <div class="form-group row">
                                  <div class="col-lg-6 col-md-9 col-sm-12" >
                                         <form action="<?php echo base_url();?>Admin/save_menu" method="post" enctype="multipart/form-data" >
                                              <div class="form-group">
                                              <select class="form-control" name="cat_id">
                                                 <option value="0">Select Category</option>
                                                 <?php foreach ($show_category as   $value) {?>
                                                  <option value="<?php echo $value->cat_id;?>"><?php echo  $value->cat_name;?></option>
                                                <?php }?> 
                                              </select>
                                            </div> 
                                             <div class="form-group">
                                            <label>Submenu</label>
                                              <select class="form-control" name="sub_cat_id">
                                                 <option value="0">Main</option>
                                                 <?php foreach ($show_category as   $value) {?>
                                                  <option value="<?php echo $value->cat_id;?>"><?php echo  $value->cat_name;?></option>
                                                <?php }?> 
                                              </select>
                                            </div>
                                            <div class="form-group">
                                              <label>Menu Position</label>
                                              <select class="form-control" name="position">
                                                  <option value="1">Header</option>
                                                  <option value="2">Footer</option>
                                              </select>
                                            </div>
                                            <div class="form-group">
                                                 <input type="checkbox" name="menu_status" value="1"/>Status                       
                                            </div>
                                             <button type="submit" name="btn" class="btn btn-info">Update</button>    
                                         </form>
                                      </div>
                                  </div>
                       
                            </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->