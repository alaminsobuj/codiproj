

            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                        	<h1>
                          <?php
        					            $message=$this->session->userdata('success');
        					            if(isset($message)){
        					              echo $message;
        					              $this->session->unset_userdata('success');
        					            } 
        					            ?>
        					        </h1>
                           <h1 class="page-header">Add Category</h1>
                            <form action="<?php echo base_url();?>Admin/save_category" method="post">
                            <div class="form-group">
            							    <label for="inputsm">Category Name </label>
            							    <input class="form-control input-sm" id="inputsm" name="cat_name" type="text">
            							  </div>
            							  <div class="form-group">
                             <label>Type Of Category</label>
                              <select class="form-control" name="cat_type">
                                  <option value="0">Main</option>
                                <?php foreach ($add as  $value) {?>
                                  <option value="<?php echo $value->cat_id;?>"><?php echo $value->cat_name;?></option>
                                 <?php }?>
                              </select>
                             </div>
                             <div class="form-group"> 
                                 <input type="checkbox" name="cat_status" value="1"/>Status                                 
                              </div>
            							   <button type="submit" class="btn btn-success" name="cat_submit">Save</button>
            							   <button type="reset" class="btn btn-danger">Reset Button</button>    
            							</form>
							 
                         </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->


