<?php
?>


            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                   <section class="content"> 

                        <div class="col-md-8">
                          <h1 class="page-header">Edit User</h1>
                           <?php 
                           $message=$this->session->userdata('error_message');
                           if(isset($message)){
                           ?>
                           <div class="alert alert-danger alert-dismissible fade in">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          <?php echo $message;?>
                        </div>
                        
                      <?php 
                        $this->session->unset_userdata('error_message');}
                      ?>
                          <!-- general form elements -->
                          <div class="box box-primary">
                            <div class="box-header with-border">
                              <h3 class="box-title">Register <i class="glyphicon glyphicon-user"></i></h3>
                            </div>
                            <!-- /.box-header -->


                            <!-- form start -->
                            <form action="<?php echo base_url();?>Admin/update_main_user/<?php echo $edit_main_user->user_id; ?>" enctype="multipart/form-data" method="post">
                             <input type="hidden" name="user_id" value="<?php echo $edit_main_user->user_id; ?>">
                             <div class="box-body">
                            <div class="form-group">        
                                <div class="row single_field">
                                    <div class="col-sm-4"><label>Full Name</label></div>
                                    <div class="col-sm-8">
                                        <input type="text" name="username" class="form-control" value="<?php echo $edit_main_user->username ?>" required="">
                                    </div>
                                </div>
                                <div class="row single_field">
                                    <div class="col-sm-4"><label>E-mail </label></div>
                                    <div class="col-sm-8">
                                        <input type="email" name="email" class="form-control" value="<?php echo $edit_main_user->email ?>" required="">
                                    </div>
                                </div>
                                <div class="row single_field">
                                    <input type="hidden" name="old_password" class="form-control" value="<?php echo $edit_main_user->password ?>">

                                    <div class="col-sm-4"><label>New Password </label></div>
                                    <div class="col-sm-8">
                                        <input type="password" name="password" class="form-control"  >
                                    </div>
                                </div>  
                                <div class="row single_field">
                                    <div class="col-sm-4"><label>Confirm PassWord</label></div>
                                    <div class="col-sm-8">
                                        <input type="password" name="confirm_password" class="form-control"  >
                                    </div>
                                </div>                
                        


                                <div class="row single_field">
                                    <div class="col-sm-4"><label>Access category </label></div>
                                    <div class="col-sm-8">
                                        <select name="type" class="form-control">
                                            <option value="">-Access category-</option>
                                            <?php
                                           foreach ($user_rol as $key => $value) {
                                      
                                            ?>
                                            
                                            <option value="<?php echo $value->user_type_id; ?>" <?php if($value->user_type_id==$edit_main_user->type){ echo "selected";}?>><?php echo $value->user_type;?></option>
                                           <?php }?>

                                        </select>
                                    </div>
                                </div>
                                <input type="hidden" name="status" value="0"/>

                                 <div class="row single_field">
                                    <div class="col-sm-4"><label>User Status </label></div>
                                    <div class="col-sm-8">
                                      <!--   <input type="text" name="status" class="form-control" value="<?php echo $edit_main_user->status ?>" required=""> -->
                                      <input type="checkbox" name="status" value="1" <?php if($edit_main_user->status==1){ echo "checked";}?>/>Status

                                    </div>
                                </div> 

                                <div class="row single_field">
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-8">
                                        <input type="submit" name="save" value="Create" class="btn btn-primary">
                                    </div>
                                </div> 
                            </div>  
                            </div>                    
                        </form>
                        </div>
                          <!-- /.box -->
                      </div>

                        
                    </section>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->

        




