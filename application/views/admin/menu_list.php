<?php 

?>



            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                     
                            <h1 class="page-header">Show All Menu</h1>
    

                          <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                DataTables Advanced Tables
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="dataTable_wrapper">
                                    <table class="table table-striped table-bordered table-hover" id="dataTables-example3">
                                        <thead>
                            
                                            <tr>
                                                <th>SL</th>
                                                <th>Category</th>
                                                
                                                <th>Menu Positon</th>
                                                <th>status</th>
                                                <th>Actions</th>
                                            </tr>
                                                
                                        </thead>
                                        <tbody>
                                            <?php 
                                              $s=1;
                                            foreach ($show_menu as   $key=>$value) {

                                                ?>
                                            <tr >
                                               
                                                <td><?php echo $s++;?></td>
                                                <td><?php echo $value->cat_name;?></td>
                                             
                                                <td><?php 
                                                if($value->position==1)
                                                  {echo "Header";}else
                                                 {echo "Footer";}


                                                ?></td>
                                                <td class="center">
                                                   <?php
                                                   if($value->menu_status==1){
                                                    ?>
                                
                                                <a href="<?php echo base_url();?>Admin/inactive_menu/<?php echo $value->menu_id;?>">
                                                    <i class="fa fa-times" aria-hidden="true"></i>Inactive</a>
                                                </td>
                                                 <?php }else{?>
                                                 <a href="<?php echo base_url();?>Admin/active_menu/<?php echo $value->menu_id;?>">
                                                    <i class="fa fa-check" aria-hidden="true"></i>active</a>
                                                </td>
                                                 <?php }?>
                                                
                                                <td class="center">
                                                <a href="<?php echo base_url();?>Admin/menu_edit/<?php echo $value->menu_id?>">
                                                <button class="btn btn-info " type="button"><i class="fa fa-paste"></i> Edit</button></a>

                                                <a href="<?php echo base_url();?>Admin/delete_menu/<?php echo $value->menu_id;?>">
                                                <button class="btn btn-info " type="button"><i class="fa fa-paste"></i> Delete</button></a></td>                                                
                                            </tr>
                                            <?php }?>
                                   
                                      
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                     
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>

                             
                         </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->


