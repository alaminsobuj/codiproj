

            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                     
                            <h1 class="page-header">All Category</h1>
    

                          <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                DataTables Advanced Tables
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="dataTable_wrapper">
                                    <table class="table table-striped table-bordered table-hover" id="dataTables-example3">
                                        <thead>
                                            <tr>
                                           
                                                <th>Cat Name</th>
                                                <th>Category Types</th>
                                                <th>Status</th>
                                               
                                                <th>Action</th>
                                                
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            foreach ($cat_settings as   $value) {
                                                //  echo "<pre>";
                                                // var_dump($value->cat_type);

                                                ?>
                                            <tr >
                                               
                                                <td><?php echo $value->cat_name;?></td>
                                                <td><?php //echo $value->sub_menu_name;
                                                    if($value->cat_type==0){
                                                    echo "Main Category";
                                                 }   
                                                 else {
                                                    echo "sub Category";
                                                 }
                                    
                                                ?></td>
                                           
                                                <td class="center">
                                                   <?php
                                                   if($value->cat_status==1){
                                                    ?>
                                
                                                <a href="<?php echo base_url();?>Admin/inactive_cat/<?php echo $value->cat_id;?>">
                                                    <i class="fa fa-times" aria-hidden="true"></i>Inactive</a>
                                                </td>
                                                 <?php }else{?>
                                                 <a href="<?php echo base_url();?>Admin/active_cat/<?php echo $value->cat_id;?>">
                                                    <i class="fa fa-check" aria-hidden="true"></i>active</a>
                                                </td>
                                                 <?php }?>
                                                
                                                <td class="center">
                                                <a href="<?php echo base_url();?>Admin/cat_edit/<?php echo $value->cat_id?>">
                                                <button class="btn btn-info " type="button"><i class="fa fa-paste"></i> Edit</button></a>

                                                <a href="<?php echo base_url();?>Admin/delete_cat/<?php echo $value->cat_id;?>">
                                                <button class="btn btn-info " type="button"><i class="fa fa-paste"></i> Delete</button></a></td>                                                
                                            </tr>
                                            <?php }?>
                                   
                                      
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                     
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>

							 
                         </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->


