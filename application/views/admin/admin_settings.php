<?php 

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Startmin - Bootstrap Admin Theme</title>

        <!-- Bootstrap Core CSS -->
        <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="<?php echo base_url();?>assets/css/metisMenu.min.css" rel="stylesheet">

        <!-- DataTables CSS -->
        <link href="<?php echo base_url();?>assets/css/dataTables/dataTables.bootstrap.css" rel="stylesheet">

        <!-- DataTables Responsive CSS -->
        <link href="<?php echo base_url();?>assets/css/dataTables/dataTables.responsive.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="<?php echo base_url();?>assets/css/startmin.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="<?php echo base_url();?>assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <div id="wrapper">

            <!-- Navigation -->
            <?php include("nav.php");?>
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Tables</h1>
                        <div class="alert alert-success">
                            <?php
                            $message=$this->session->userdata('success');
                            if(isset($message)){
                              echo $message;
                              $this->session->unset_userdata('success');
                            }else{
                                echo "Welcome to User Table";
                            } 

                           ?>
                        </div>
         
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                DataTables Advanced Tables
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="dataTable_wrapper">
                                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                        <thead>
                                            <tr>
                                                <th>User Name</th>
                                                <th>Email</th>
                                                <th>Password</th>
                                                <th>User Type</th>
                                                <th>Activation</th>
                                                <th>Action</th>
                                                
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($user_settings as   $value) {
                                                
                                             ?>
                                            <tr >
                                                <td><?php echo $value->username;?></td>
                                                <td><?php echo $value->email?></td>
                                                <td><?php echo $value->password?></td>
                                                <td class="center"><?php echo $value->type?></td> 
                                                <td class="center">
                                                     <?php
                                                    if ($value->status == 1) {
                                                ?>
                                                <a href="<?php echo base_url();?>Admin/inactive_user/<?php echo $value->user_id ?>">
                                                    <i class="fa fa-times" aria-hidden="true"></i>Inactive</a>
                                                </td>
                                                <?php        
                                                    }else{
                                                ?>
                                                 <a href="<?php echo base_url();?>Admin/active_user/<?php echo $value->user_id ?>">
                                                    <i class="fa fa-check" aria-hidden="true"></i>Active</a>
                                                </td>
                                                <?php
                                                    }
                                                ?>
                                                  
                                                </td>
                                                <td class="center">
                                                <a href="<?php echo base_url();?>Admin/edit_user/<?php echo $value->user_id ?>">
                                                <button class="btn btn-info " type="button"><i class="fa fa-paste"></i> Edit</button></a>

                                                <a href="<?php echo base_url();?>Admin/delete_user/<?php echo $value->user_id ?>">
                                                <button class="btn btn-info " type="button"><i class="fa fa-paste"></i> Delete</button></a></td>                                                 
                                            </tr>
                                          <?php  }?>
                                      
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                     
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
           
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->

        <!-- jQuery -->
        <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="<?php echo base_url();?>assets/js/metisMenu.min.js"></script>

        <!-- DataTables JavaScript -->
        <script src="<?php echo base_url();?>assets/js/dataTables/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/dataTables/dataTables.bootstrap.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="<?php echo base_url();?>assets/js/startmin.js"></script>

        <!-- Page-Level Demo Scripts - Tables - Use for reference -->
        <script>
            $(document).ready(function() {
                $('#dataTables-example').DataTable({
                        responsive: true
                });
            });
        </script>

    </body>
</html>
