            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">Add Posts</h1>
                                <div class="form-group row">
                                  <div class="col-lg-6 col-md-9 col-sm-12" >
                                         <form action="<?php echo base_url();?>Admin/menu_edit_save" method="post" enctype="multipart/form-data" >
                                           <input type="hidden" name="menu_id" value="<?php echo $menu_edit->menu_id ?>">
                                           
                                              <div class="form-group">
                                              <select class="form-control" name="cat_id">
                                                 <option value="0">Select Category</option>
                                                 <?php foreach ($show_category as   $value) {
                                                  ?>
                                                  <option value="<?php echo $value->cat_id;?>" <?php  if($value->cat_id==$menu_edit->cat_id){echo "selected";}  ?>><?php echo  $value->cat_name;?></option>
                                                <?php }?> 
                                              </select>
                                            </div> 
                                             <div class="form-group">
                                            <label>Submenu</label>
                                              <select class="form-control" name="sub_cat_id">
                                                
                                              
                                                 <?php if($menu_edit->sub_cat_id){?>
                                                 <?php foreach ($show_category as   $value) {?>
                                                  <option value="<?php echo $value->cat_id;?>"><?php echo  $value->cat_name;?></option>
                                                <?php }}else{?>
                                                   <option value="0">Main</option>
                                                <?php }?> 
                                              </select>
                                            </div>
                                            <div class="form-group">
                                              <label>Menu Position</label>
                                              <select class="form-control" name="position">
                                                
                                                      <option value="1"   <?php if($menu_edit->position==1){ echo "selected";}?>>Header</option>
                                                      <?php 
                                                      ?>
                                                    <option value="2" <?php if($menu_edit->position==2){ echo "selected";}?>>Footer</option>
                                              </select>
                                            </div>
                                            <div class="form-group">
                                                 <input type="checkbox" name="menu_status" value="1" <?php if($menu_edit->menu_status==1){ echo "checked";}?>/>Status                       
                                            </div>
                                             <button type="submit" name="btn" class="btn btn-info">Update</button>    
                                         </form>
                                      </div>
                                  </div>
                       
                            </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->