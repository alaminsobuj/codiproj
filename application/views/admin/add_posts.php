<?php
?>


            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">Add Posts</h1>
                          
                                <div class="form-group row">
                                     
                                  <div class="col-lg-6 col-md-9 col-sm-12" >
                                         <form action="<?php echo base_url();?>Admin/save_posts" method="post" enctype="multipart/form-data" >
                                          
                                              <div class="form-group">
                                            
                                              <select class="form-control" name="cat_id">
                                                 <option value="0">Select Category</option>
                                                 <?php foreach ($show_category as   $value) {?>
                                                 
                                                  <option value="<?php echo $value->cat_id;?>"><?php echo  $value->cat_name;?></option>
                                                <?php }?> 
                                                
                                              </select>
                                           
                                            </div>
                                      <div class="form-group">
                                           <input type="text" name="title" placeholder="News title" class="form-control">
                                      </div>  

                                      <div class="form-group">
                                          <textarea class="form-control" name="short_description" placeholder="Short Discription"></textarea>
                                      </div>   

                                      <div class="form-group">
                                         <input type="file" name="post_image">
                                      </div>

                                      <div class="form-group">
                                        <textarea name="long_description" id="editor1" rows="10" cols="80" ></textarea>       
                                      </div>  

                                      <div class="form-group">
                                          <input id="tags" type="text" name="tag" class=" form-control" placeholder="Tags" >
                                      </div> 

                                        
                                    <div class="form-group">
                                      <input id="tags" type="text" name="link" class=" form-control" placeholder="link" >
                                    </div>
                                    <div class="form-group">
                                             <input type="checkbox" name="status" value="1"/>Status                             
                                    </div>
                                

                                             <button type="submit" name="btn" class="btn btn-info">Update</button>    
                                         </form>
                                      </div>
                                  </div>
                                      <script>
                                  CKEDITOR.replace( 'editor1', {
                                    // fullPage: true,
                                    // extraPlugins: 'docprops',
                                    // Disable content filtering because if you use full page mode, you probably
                                    // want to  freely enter any HTML content in source mode without any limitations.
                                    // allowedContent: true,
                                    // height: 320
                                  } );
                                </script>
                          
                                         

                            </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->

        




