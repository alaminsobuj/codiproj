<?php 

?>



            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                     
                            <h1 class="page-header">Show All Menu</h1>
                            

                          <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                DataTables Advanced Tables
                            </div>
                             <?php 
                           $message=$this->session->userdata('success');
                           if(isset($message)){
                           ?>
                           <div class="alert alert-success alert-dismissible fade in">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          <?php echo $message;?>
                        </div>
                        
                      <?php 
                        $this->session->unset_userdata('success');}
                      ?>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="dataTable_wrapper">
                                    <table class="table table-striped table-bordered table-hover" id="dataTables-example3">
                                        <thead>
                            
                                            <tr>
                                                <th>SL</th>
                                                <th>User Name</th>
                                                
                                                <th>Email</th>
                                                <th>type</th>
                                                <th>Status</th>
                                                <th>Actions</th>
                                            </tr>
                                                
                                        </thead>
                                        <tbody>
                                            <?php 
                                              $s=1;
                                            foreach ($view_user_list as   $key=>$value) {

                                                ?>
                                            <tr >
                                               
                                                <td><?php echo $s++;?></td>
                                                <td><?php echo $value->username;?></td>
                                                <td><?php echo $value->email;?></td>
                                                <td><?php echo $value->user_type;?></td>
                                        
                                                <td class="center">
                                                   <?php
                                                   if($value->status==1){
                                                    ?>
                                
                                                <a href="<?php echo base_url();?>Admin/inactive_user_id/<?php echo $value->user_id?>">
                                                    <i class="fa fa-times" aria-hidden="true"></i>Inactive</a>
                                                </td>
                                                 <?php }else{?>
                                                 <a href="<?php echo base_url();?>Admin/active_user_id/<?php echo $value->user_id?>">
                                                    <i class="fa fa-check" aria-hidden="true"></i>active</a>
                                                </td>
                                                 <?php }?>
                                                
                                                <td class="center">
                                                <a href="<?php echo base_url();?>Admin/edit_main_user/<?php echo $value->user_id;?>">
                                                <button class="btn btn-info " type="button"><i class="fa fa-paste"></i> Edit</button></a>

                                                <a href="<?php echo base_url();?>Admin/delete_main_user/<?php echo $value->user_id;?>">
                                                <button class="btn btn-info " type="button"><i class="fa fa-paste"></i> Delete</button></a></td>                                                
                                            </tr>
                                            <?php }?>
                                   
                                      
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                     
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>

                             
                         </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->


