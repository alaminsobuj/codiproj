<?php
?>


            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">Add Posts</h1>
                          <?php 
                          
                           
                            ?>
                                <div class="form-group row">
                                     
                                  <div class="col-lg-6 col-md-9 col-sm-12" >
                                         <form action="<?php echo base_url();?>Admin/update_posts" method="post" enctype="multipart/form-data" >
                                          <input type="hidden" name="post_id" value="<?php echo $post_edit->post_id?>">
                                              <div class="form-group">
                                            
                                              <select class="form-control" name="cat_id">
                                            
                                                 <option value="0">Select Category</option>
                                                 <?php foreach ($show_category as   $value) {
                                                  ?>
                                                 
                                            <option value="<?php echo $value->cat_id;?>" <?php if($value->cat_id==$post_edit->post_id){echo "selected";}?>><?php echo  $value->cat_name;?></option>
                                                <?php }?> 
                                              </select>
                                           
                                            </div>
                                      <div class="form-group">
                                           <input type="text" name="title" placeholder="News title" class="form-control" value="<?php echo $post_edit->title;?>">
                                      </div>  

                                      <div class="form-group">
                                          <textarea class="form-control" name="short_description" placeholder="Short Discription"><?php echo $post_edit->short_description;?></textarea>
                                      </div>   

                                      <div class="form-group">
                                         <input type="file" name="post_image" >
                                         <input type="hidden" name="old_image" value="<?php echo $post_edit->post_image;?>" >

                                         <img src="<?php echo base_url().'assets/uploads/post_image/'.$post_edit->post_image;?>" height="100px" width="100px">
                                      </div>

                                      <div class="form-group">
                                        <textarea name="long_description" id="editor1" rows="10" cols="80" ><?php echo $post_edit->long_description;?></textarea>       
                                      </div>  

                                      <div class="form-group">
                                          <input id="tags" type="text" name="tag" class=" form-control" placeholder="Tags" value="<?php echo $post_edit->tag;?>" >
                                      </div> 

                                        
                                    <div class="form-group">
                                      <input id="tags" type="text" name="link" class=" form-control" placeholder="link" value="<?php echo $post_edit->link;?>" >
                                    </div>
                                    <div class="form-group">
                                           
                                             <input type="checkbox" name="status" value="1" <?php if($post_edit->status==1){ echo "checked";}?>/>Status

                                    </div>
                                

                                             <button type="submit" name="btn" class="btn btn-info">Update</button>    
                                         </form>
                                      </div>
                                  </div>
                                      <script>
                                  CKEDITOR.replace( 'editor1', {
                                    // fullPage: true,
                                    // extraPlugins: 'docprops',
                                    // Disable content filtering because if you use full page mode, you probably
                                    // want to  freely enter any HTML content in source mode without any limitations.
                                    // allowedContent: true,
                                    // height: 320
                                  } );
                                </script>
                          
                                         

                            </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->

        




