<?php

?>

<div class="section">
			<!-- CONTAINER -->
			<div class="container">
				<!-- ROW -->
				<div class="row">
					<!-- Main Column -->
					<div class="col-md-8">

						
						<!-- ARTICLE -->
						<?php foreach ($show_post_list as $key => $value){
                       
                             
                             
          
							?>
						<article class="article row-article">
							<div class="article-img">
								<a href="#">
								<!-- 	<img src="<?php echo base_url();?>uploads/post_image/<?php echo $value->post_image;?>" alt=""> -->
									 <img src="<?php echo base_url();?>assets/uploads/post_image/<?php echo $value->post_image;?>">
								</a>
							</div>
							<div class="article-body">
								<ul class="article-info">
									<li class="article-category">
										<a href="<?php echo base_url();?>Welcome/post_details/<?php echo $value->post_id;?>"><?php echo $value->cat_name;?></a></li>
									<li class="article-type"><i class="fa fa-file-text"></i></li>
								</ul>
								<h3 class="article-title"><a href="#"><?php echo $value->title;?></a></h3>
								<ul class="article-meta">
									<li><i class="fa fa-clock-o"></i> January 31, 2017</li>
									<li><i class="fa fa-comments"></i> 33</li>
								</ul>
								<p><?php echo $value->short_description;?></p>
							</div>
						</article>
						<!-- /ARTICLE -->
						<?php }?>
		              
						
						<!-- pagination -->
						<div class="article-pagination">
							<!-- <ul> 
								<li class="active"><a href="#" class="active">1</a></li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#"><i class="fa fa-angle-right"></i></a></li>
							</ul> -->
						 <?php echo $links;?>	
						</div>
						<!-- /pagination -->
					</div>
					<!-- /Main Column -->
					
					<!-- Aside Column -->
					<div class="col-md-4">
						<!-- article widget -->
						<div class="widget">
							<div class="widget-title">
								<h2 class="title">Featured Posts</h2>
							</div>
							
							<!-- owl carousel 4 -->
							<div id="owl-carousel-4" class="owl-carousel owl-theme owl-loaded owl-drag">
								<!-- ARTICLE -->
								
								<!-- /ARTICLE -->
							
								<!-- ARTICLE -->
								
								<!-- /ARTICLE -->
							<div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(-1080px, 0px, 0px); transition: 0.25s; width: 2160px;"><div class="owl-item cloned" style="width: 360px;"><article class="article thumb-article">
									<div class="article-img">
										<img src="<?php echo base_url();?>assets/frond_part/img/img-thumb-1.jpg" alt="">
									</div>
									<div class="article-body">
										<ul class="article-info">
											<li class="article-category"><a href="#">News</a></li>
											<li class="article-type"><i class="fa fa-video-camera"></i></li>
										</ul>
										<h3 class="article-title"><a href="#">Duis urbanitas eam in, tempor consequat.</a></h3>
										<ul class="article-meta">
											<li><i class="fa fa-clock-o"></i> January 31, 2017</li>
											<li><i class="fa fa-comments"></i> 33</li>
										</ul>
									</div>
								</article></div><div class="owl-item cloned" style="width: 360px;"><article class="article thumb-article">
									<div class="article-img">
										<img src="<?php echo base_url();?>assets/frond_part/img/img-thumb-2.jpg" alt="">
									</div>
									<div class="article-body">
										<ul class="article-info">
											<li class="article-category"><a href="#">News</a></li>
											<li class="article-type"><i class="fa fa-video-camera"></i></li>
										</ul>
										<h3 class="article-title"><a href="#">Duis urbanitas eam in, tempor consequat.</a></h3>
										<ul class="article-meta">
											<li><i class="fa fa-clock-o"></i> January 31, 2017</li>
											<li><i class="fa fa-comments"></i> 33</li>
										</ul>
									</div>
								</article></div><div class="owl-item" style="width: 360px;"><article class="article thumb-article">
									<div class="article-img">
										<img src="<?php echo base_url();?>assets/frond_part/img/img-thumb-1.jpg" alt="">
									</div>
									<div class="article-body">
										<ul class="article-info">
											<li class="article-category"><a href="#">News</a></li>
											<li class="article-type"><i class="fa fa-video-camera"></i></li>
										</ul>
										<h3 class="article-title"><a href="#">Duis urbanitas eam in, tempor consequat.</a></h3>
										<ul class="article-meta">
											<li><i class="fa fa-clock-o"></i> January 31, 2017</li>
											<li><i class="fa fa-comments"></i> 33</li>
										</ul>
									</div>
								</article></div><div class="owl-item active" style="width: 360px;"><article class="article thumb-article">
									<div class="article-img">
										<img src="<?php echo base_url();?>assets/frond_part/img/img-thumb-2.jpg" alt="">
									</div>
									<div class="article-body">
										<ul class="article-info">
											<li class="article-category"><a href="#">News</a></li>
											<li class="article-type"><i class="fa fa-video-camera"></i></li>
										</ul>
										<h3 class="article-title"><a href="#">Duis urbanitas eam in, tempor consequat.</a></h3>
										<ul class="article-meta">
											<li><i class="fa fa-clock-o"></i> January 31, 2017</li>
											<li><i class="fa fa-comments"></i> 33</li>
										</ul>
									</div>
								</article></div><div class="owl-item cloned" style="width: 360px;"><article class="article thumb-article">
									<div class="article-img">
										<img src="<?php echo base_url();?>assets/frond_part/img/img-thumb-1.jpg" alt="">
									</div>
									<div class="article-body">
										<ul class="article-info">
											<li class="article-category"><a href="#">News</a></li>
											<li class="article-type"><i class="fa fa-video-camera"></i></li>
										</ul>
										<h3 class="article-title"><a href="#">Duis urbanitas eam in, tempor consequat.</a></h3>
										<ul class="article-meta">
											<li><i class="fa fa-clock-o"></i> January 31, 2017</li>
											<li><i class="fa fa-comments"></i> 33</li>
										</ul>
									</div>
								</article></div><div class="owl-item cloned" style="width: 360px;"><article class="article thumb-article">
									<div class="article-img">
										<img src="<?php echo base_url();?>assets/frond_part/img/img-thumb-2.jpg" alt="">
									</div>
									<div class="article-body">
										<ul class="article-info">
											<li class="article-category"><a href="#">News</a></li>
											<li class="article-type"><i class="fa fa-video-camera"></i></li>
										</ul>
										<h3 class="article-title"><a href="#">Duis urbanitas eam in, tempor consequat.</a></h3>
										<ul class="article-meta">
											<li><i class="fa fa-clock-o"></i> January 31, 2017</li>
											<li><i class="fa fa-comments"></i> 33</li>
										</ul>
									</div>
								</article></div></div></div><div class="owl-nav disabled"><div class="owl-prev">prev</div><div class="owl-next">next</div></div><div class="owl-dots"><div class="owl-dot"><span></span></div><div class="owl-dot active"><span></span></div></div></div>
							<!-- /owl carousel 4 -->
						</div>
						<!-- /article widget -->
						
						<!-- galery widget -->
						<div class="widget galery-widget">
							<div class="widget-title">
								<h2 class="title">Flickr Photos</h2>
							</div>
							<ul>
								<li><a href="#"><img src="<?php echo base_url();?>assets/frond_part/img/img-widget-3.jpg" alt=""></a></li>
								<li><a href="#"><img src="<?php echo base_url();?>assets/frond_part/img/img-widget-4.jpg" alt=""></a></li>
								<li><a href="#"><img src="<?php echo base_url();?>assets/frond_part/img/img-widget-5.jpg" alt=""></a></li>
								<li><a href="#"><img src="<?php echo base_url();?>assets/frond_part/img/img-widget-6.jpg" alt=""></a></li>
								<li><a href="#"><img src="<?php echo base_url();?>assets/frond_part/img/img-widget-7.jpg" alt=""></a></li>
								<li><a href="#"><img src="<?php echo base_url();?>assets/frond_part/img/img-widget-8.jpg" alt=""></a></li>
								<li><a href="#"><img src="<?php echo base_url();?>assets/frond_part/img/img-widget-9.jpg" alt=""></a></li>
								<li><a href="#"><img src="<?php echo base_url();?>assets/frond_part/img/img-widget-10.jpg" alt=""></a></li>
							</ul>
						</div>
						<!-- /galery widget -->
						
						<!-- tweets widget -->
						<div class="widget tweets-widget">
							<div class="widget-title">
								<h2 class="title">Recent Tweets</h2>
							</div>
							<ul>
								<li class="tweet">
									<i class="fa fa-twitter"></i>
									<div class="tweet-body">
										<p><a href="#">@magnews</a> Populo tritani laboramus ex mei, no eum iuvaret ceteros euripidis <a href="#">https://t.co/DwsTbsmxTP</a></p>
									</div>
								</li>
								<li class="tweet">
									<i class="fa fa-twitter"></i>
									<div class="tweet-body">
										<p><a href="#">@magnews</a> Populo tritani laboramus ex mei, no eum iuvaret ceteros euripidis <a href="#">https://t.co/DwsTbsmxTP</a></p>
									</div>
								</li>
								<li class="tweet">
									<i class="fa fa-twitter"></i>
									<div class="tweet-body">
										<p><a href="#">@magnews</a> Populo tritani laboramus ex mei, no eum iuvaret ceteros euripidis <a href="#">https://t.co/DwsTbsmxTP</a></p>
									</div>
								</li>
							</ul>
						</div>
						<!-- /tweets widget -->
					</div>
					<!-- /Aside Column -->
				</div>
				<!-- /ROW -->
			</div>
			<!-- /CONTAINER -->
		</div>