<?php

?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		 <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

		<title>Magnews HTML Template</title>

		<!-- Google font -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700%7CLato:300,400" rel="stylesheet"> 
		
		<!-- Bootstrap -->
		<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/frond_part/css/bootstrap.min.css"/>

		<!-- Owl Carousel -->
		<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/frond_part/css/owl.carousel.css" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/frond_part/css/owl.theme.default.css" />
		
		<!-- Font Awesome Icon -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/frond_part/css/font-awesome.min.css">

		<!-- Custom stlylesheet -->
		<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/frond_part/css/style.css"/>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
<style type="text/css">
/*	.dropdown-menu>li>a:focus, .dropdown-menu>li>a:hover {
    color: #fff !important;
}
.dropdown-menu>li>a{
	color: #222!important;
	border-bottom: 1px solid #222;
   line-height: 12px;
}
.btn-primary.active, .btn-primary:active, .open>.dropdown-toggle.btn-primary{
	background-color: #EF233C!important;
}
.btn-primary {
    color: #fff;
    background-color: #333;
 
}
*/

.dropbtn {
    /*background-color: #4CAF50;*/
    color: white;
    padding: 16px;
    font-size: 16px;
    border: none;
}

.dropdown {
    position: relative;
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #2C2C2C;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

.dropdown-content a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
}
.dropdown-content a:hover {background-color: #ddd}

.dropdown:hover .dropdown-content {
    display: block;
        z-index: 2;
}

.dropdown:hover .dropbtn {
    /*background-color: #3e8e41;*/
}
</style>
    </head>
	<body>
	
		<!-- Header -->
		<header id="header">
			<!-- Top Header -->
			<div id="top-header">
				<div class="container">
					<div class="header-links">
						<ul>

							<li><a href="#">About us</a></li>
							<li><a href="#">Contact</a></li>
							<li><a href="#">Advertisement</a></li>
							<li><a href="#">Privacy</a></li>
							<li><a href="#"><i class="fa fa-sign-in"></i> Login</a></li>
						</ul>
					</div>
					<div class="header-social">
						<ul>
							<li><a href="#"><i class="fa fa-facebook"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
							<li><a href="#"><i class="fa fa-instagram"></i></a></li>
							<li><a href="#"><i class="fa fa-youtube"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
			<!-- /Top Header -->
			
			<!-- Center Header -->
			<div id="center-header">
				<div class="container">
					<div class="header-logo">
						<a href="<?php echo base_url();?>" class="logo"><img src="<?php echo base_url();?>assets/frond_part/img/logo.png" alt=""></a>
					</div>
					<div class="header-ads">
						<img class="center-block" src="<?php echo base_url();?>assets/frond_part/img/ad-2.jpg" alt=""> 
					</div>
				</div>
			</div>
			<!-- /Center Header -->
			
			<!-- Nav Header -->
			<div id="nav-header">
				<div class="container">
					<nav id="main-nav">
						<div class="nav-logo">
							<a href="" class="logo"><img src="<?php echo base_url();?>assets/frond_part/img/logo-alt.png" alt=""></a>
						</div>

						<ul class="main-nav nav navbar-nav">
							<li class="active"><a href="<?php echo base_url();?>">Home</a></li> 
							
                         
                         <?php 
						
							?>
							<?php foreach ($show_category as $key => $value) { 
							if($value->cat_type){?>
                         <li class="dropdown">
                            <!-- <div class="dropdown"> -->
							  <a class="dropbtn" href="<?php echo base_url();?>Welcome/cateory/<?php echo $value->cat_id;?>"><?php echo $value->cat_name;?>
							  </a>
							  <ul class="dropdown-content">
							  	<?php foreach ($sub_category as $key => $values) {
							  		if($value->cat_id==$values->cat_type){
							  		?>
							         <li><a href="<?php echo base_url();?>Welcome/cateory/<?php echo $values->cat_id;?>"><?php echo  $values->cat_name?></a></li>
							    <?php
									} 
								}
								?>
							  </ul>
							  
							<!-- </div> -->
                         </li>
                         <?php }else{ ?>
                          <li><a href="<?php echo base_url();?>Welcome/cateory/<?php echo $value->cat_id;?>"><?php echo  $value->cat_name?></a></li>
                         <?php
							}}
							 
							?>
							
						</ul>
					</nav>
					<div class="button-nav">
						<button class="search-collapse-btn"><i class="fa fa-search"></i></button>
						<button class="nav-collapse-btn"><i class="fa fa-bars"></i></button>
						<div class="search-form">
							<form>
								<input class="input" type="text" name="search" placeholder="Search">
							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- /Nav Header -->
		</header>
		<!-- /Header -->