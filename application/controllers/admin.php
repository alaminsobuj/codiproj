<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

   function __construct() {
        // then execute the parent constructor anyway
        parent::__construct();
        $this->load->model('Admin_login_model');
    }
    
	public function index()
	{
		$this->load->helper('form');
		$this->load->view('admin/admin_login');

	}

    public function admin_login(){

    $this->load->library('form_validation');
    $this->form_validation->set_rules('username','User Name','required|alpha|trim');
    $this->form_validation->set_rules('password','Password','required');
    $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
               if ($this->form_validation->run())
                {
                 $username=$this->input->post('username',true);
                  $password=$this->input->post('password',true);
                  $result= $this->Admin_login_model->check_validation($username,$password);
                           if($result){
                            $sdata=array();
                                $sdata['user_id']=$result->user_id;
                                $sdata['username']=$result->username;
                                $sdata['email']=$result->email;
                                $sdata['password']=$result->password;
                                $sdata['type']=$result->type;
                                $sdata['status']=$result->status;

                               $this->session->set_userdata($sdata);
                               if($sdata['status']==1){
                                    if($sdata['type']==1){
                                    $sdata['main_content'] = $this->load->view('admin/dashboard','',true);
                                    $this->load->view('admin/mastering',$sdata);
                                       // $this->load->view('admin/dashboard');
                                       // $this->load->view('admin/nav');
                                       }
                                       elseif($sdata['type']==2){
                                             echo "You Are A Author";
                                       }
                                       else{
                                        echo "U are Normal User";
                                       }
                              }else{
                                echo "You Are Not Active User Please Waite";
                              }         

                           }
                           else{
                                $sdata['message']='Please Insert valid email Or Password';
                                $this->session->set_userdata($sdata);
                                
                                redirect("admin/admin_login");

                               }
                }
                else
                {
                      // echo "error";
                      $this->load->view('admin/admin_login');
                }


    }
public function logout(){
   $this->session->unset_userdata('user_id');
redirect("Admin/admin_login",'refresh');
 }
public function user_profiles(){
  $data=array();
  $data['user_profiles']=$this->Admin_login_model->view_user_info();
  $this->load->view('admin/admin_profile',$data);
}

// all user show
public function user_settings(){
$data=array();
$data['user_settings']=$this->Admin_login_model->view_all_user();
 $this->load->view('admin/admin_settings',$data);
// echo "<pre>";
// var_dump($data);
}
public function edit_user($user_id){
     $data=array();
     $data['edit_user']=$this->Admin_login_model->update_user_info($user_id);          
     $this->load->view('admin/admin_edit_page',$data);

}

public function update_user_info(){
  $data=array();
  $data['user_id']=$this->input->post('user_id',true);
  $data['username']=$this->input->post('username',true);
  $data['email']=$this->input->post('email',true);
  $data['password']=$this->input->post('password',true);
  $data['type']=$this->input->post('type',true);
$this->Admin_login_model->update_user_content($data);
$sdata=array();
$sdata['success']="Your information Successfully save !";
$this->session->set_userdata($sdata);
redirect('Admin/user_settings');
}

public function active_user($user_id){ 
  $data=array();
  $data['user_id']=$user_id;
  $data['status']=1;
  $this->db->where('user_id',$data['user_id']);
  $this->db->update('tbl_user',$data); 
redirect('Admin/user_settings');
}

public function inactive_user($user_id){
  $data=array();
  $data['user_id']=$user_id;
  $data['status']=0;
  $this->db->where('user_id',$data['user_id']);
  $this->db->update('tbl_user',$data);
redirect('Admin/user_settings');
}

// delete
public function delete_user($user_id){
  $data=array();
  $data['user_id']=$user_id;
  var_dump($data);
  $this->db->where('user_id',$data['user_id']);
  $this->db->delete('tbl_user',$data);
redirect('Admin/user_settings');
}
// dashboard home urldecode
public function admn_dashboard(){
  $sdata['main_content'] = $this->load->view('admin/dashboard','',true);
  $this->load->view('admin/mastering',$sdata);
    // $this->load->view('admin/dashboard');
}
// add category  
public function add_category(){
  $data['add']=$this->Admin_login_model->cat_view();
  $data['main_content'] = $this->load->view('admin/add_category',$data,true);
  $this->load->view('admin/mastering',$data);
  // $this->load->view('',$data);
 }
public function save_category(){
  $data=array();
  $data['cat_name']=$this->input->post('cat_name',true);
  $data['cat_type']=$this->input->post('cat_type',true);
  $status=$this->input->post('cat_status',true);
    if($status==1){
       $data['cat_status']= $status;
    }
    else{
       $data['cat_status']= 0;
    }
 $this->Admin_login_model->insert_cat($data);
 $sdata=array();
 $sdata['success']="Your information Success Fully save !";
 $this->session->set_userdata($sdata);
 redirect('Admin/add_category');
}

// all category show 
public function cat_settings(){
$data=array();
$data['cat_settings']=$this->Admin_login_model->view_cat_settings();
$data['main_content'] = $this->load->view('admin/cat_settings',$data,true);
$this->load->view('admin/mastering',$data);
// $this->load->view('admin/cat_settings',$data);
}
public function active_cat($cat_id){ 
  $data=array();
  $data['cat_id']=$cat_id;
  $data['cat_status']=1;
  $this->db->where('cat_id',$data['cat_id']);
  $this->db->update('tbl_cat',$data);
redirect('admin/cat_settings');
}
public function inactive_cat($cat_id){ 
  $data=array();
  $data['cat_id']=$cat_id;
  $data['cat_status']=0;
  $this->db->where('cat_id',$data['cat_id']);
  $this->db->update('tbl_cat',$data);
redirect('admin/cat_settings');
}

// edit category 
function cat_edit($cat_id){
$data=array();
$data['cat_edit']=$this->Admin_login_model->cat_edit($cat_id);   
$data['add']=$this->Admin_login_model->cat_view();
$data['main_content'] = $this->load->view('admin/edit_cat',$data,true);
$this->load->view('admin/mastering',$data);
}

function update_category($cat_id){
  $data=array();
  $data['cat_id']=$this->input->post('cat_id',true);
  $data['cat_name']=$this->input->post('cat_name',true);
  $data['cat_type']=$this->input->post('cat_type',true);
  $status=$this->input->post('cat_status',true);
    if($status==1){
       $data['cat_status']= $status;
    }
    else{
       $data['cat_status']= 0;
    }
 $this->Admin_login_model->update_cat($data);
 $sdata=array();
 $sdata['success']="Your information Success Fully save !";
 $this->session->set_userdata($sdata);
 redirect('Admin/cat_edit/'.$cat_id);

}

// delete

public function delete_cat($cat_id){
  $data=array();
  $data['cat_id']=$cat_id;
  $this->db->where('cat_id',$data['cat_id']);
  $this->db->delete('tbl_cat',$data);
  redirect('Admin/cat_settings');
}
public function add_posts(){
$data['show_category']=$this->Admin_login_model->show_post_category();
$data['main_content'] = $this->load->view('admin/add_posts',$data,true);
$this->load->view('admin/mastering',$data);
 // $this->load->view('admin/add_posts');
}
// post save  
function save_posts(){
   $config['allowed_types'] = '*';
   $config['remove_spaces'] = true;
  if (isset($_FILES['post_image']['name'])){
  $config['upload_path'] = 'assets/uploads/post_image/';
  $this->load->library('upload', $config);
       if($this->upload->do_upload('post_image')){
        $upload_data = $this->upload->data();
        $data['post_image'] = $upload_data['file_name'];
       }
       else{
        $upload_data = "";
        $data['post_image'] = $upload_data; 
       }
  } 
    $data['cat_id']=$this->input->post('cat_id');
    $data['title']=$this->input->post('title');
    $data['short_description']=$this->input->post('short_description');
    $data['long_description']=$this->input->post('long_description');
    $data['long_description']=$this->input->post('long_description');
    $data['tag']=$this->input->post('tag');
    $data['link']=$this->input->post('link');
    $data['date']= date("Y-m-d");
    $status=$this->input->post('status');
     if($status==1){
     $data['status']= $status;
      }
     else{
      $data['status']= 0;
     }
 $this->Admin_login_model->insert_post($data);
 redirect('Admin/post_list');
}

// post list 
function post_list(){
  $data=array();
 $data['show_post_list']=$this->Admin_login_model->show_post_list($data);
 $data['show_category']=$this->Admin_login_model->show_post_category();
 $data['main_content'] = $this->load->view('admin/post_list',$data,true);
 $this->load->view('admin/mastering',$data);
}
// post active inactive 
function pending($post_id){
  $data=array();
  $data['post_id']=$post_id;
  $data['status']=0;
  $this->db->where('post_id',$data['post_id']);
  $this->db->update('tbl_post',$data);
  redirect('Admin/post_list');
}
function published($post_id){
  $data=array();
  $data['post_id']=$post_id;
  $data['status']=1;
  $this->db->where('post_id',$data['post_id']);
  $this->db->update('tbl_post',$data);   
  redirect('Admin/post_list');
 }
// end post active inactive

// delete post
public function delete_post($post_id){
  $data=array();
  $data['post_id']=$post_id;
  $this->db->where('post_id',$data['post_id']);
  $this->db->delete('tbl_post',$data);
  redirect('Admin/post_list');
}
// end delete post

// edit post 
function edit_post($post_id){
$data=array();
$data['post_edit']=$this->Admin_login_model->post_edit($post_id);
$data['show_category']=$this->Admin_login_model->show_post_category();
$data['main_content'] = $this->load->view('admin/edit_post',$data,true);
$this->load->view('admin/mastering',$data);

}
// end post 

function update_posts(){

   $config['allowed_types'] = '*';
   $config['remove_spaces'] = true;
  if (isset($_FILES['post_image']['name'])){
  $config['upload_path'] = 'assets/uploads/post_image/';
  $this->load->library('upload', $config);
       if($this->upload->do_upload('post_image')){
        $upload_data = $this->upload->data();
        $data['post_image'] = $upload_data['file_name'];
       }
       else{
        $data['post_image'] = $this->input->post('old_image'); 
       }
  } 
    $data['post_id']=$this->input->post('post_id',true);
    $data['cat_id']=$this->input->post('cat_id');
    $data['title']=$this->input->post('title');
    $data['short_description']=$this->input->post('short_description');
    $data['long_description']=$this->input->post('long_description');
    $data['long_description']=$this->input->post('long_description');
    $data['tag']=$this->input->post('tag');
    $data['link']=$this->input->post('link');
        $status=$this->input->post('status');
         if($status==1){$data['status']= $status;}
         else{$data['status']= 0;}
$data['update_post']=$this->Admin_login_model->update_post($data);
redirect('Admin/post_list');

}




function add_menu(){
   $data=array();
   $data['show_category']=$this->Admin_login_model->show_post_category();
   $data['main_content'] = $this->load->view('admin/add_menu',$data,true);
   $this->load->view('admin/mastering',$data);
  }
function menu_list_page(){
   $data=array();
   $data['show_category']=$this->Admin_login_model->show_post_category();
   $data['show_menu']=$this->Admin_login_model->view_menu_list();
   $data['main_content'] = $this->load->view('admin/menu_list',$data,true);
   $this->load->view('admin/mastering',$data);
  }

function save_menu(){
    $data=array();
    $data['cat_id']=$this->input->post('cat_id',true);
    $data['sub_cat_id']=$this->input->post('sub_cat_id',true);
    $data['position']=$this->input->post('position',true);
    $status=$this->input->post('menu_status',true);
      if($status==1){
         $data['menu_status']= $status;
      }
      else{
         $data['menu_status']= 0;
      }
   $menu=$this->Admin_login_model->insert_menu_db($data);
       $sdata=array();
       if($menu){
         $sdata['megs']="This Menu is Successfully Insert";
       }else{
         $sdata['megs']="This Menu is Successfully Insert";
       } 
     
  redirect('Admin/menu_list_page');
  }


public function active_menu($menu_id){ 
  $data=array();
  $data['menu_id']=$menu_id;
  $data['menu_status']=1;
  $this->db->where('menu_id',$data['menu_id']);
  $this->db->update('tbl_menu',$data);
redirect('admin/menu_list_page');
}
public function inactive_menu($menu_id){ 
  $data=array();
  $data['menu_id']=$menu_id;
  $data['menu_status']=0;
  $this->db->where('menu_id',$data['menu_id']);
  $this->db->update('tbl_menu',$data);
redirect('admin/menu_list_page');
}

public function delete_menu($menu_id){
  $data=array();
  $data['menu_id']=$menu_id;
  $this->db->where('menu_id',$data['menu_id']);
  $this->db->delete('tbl_menu',$data);
redirect('admin/menu_list_page');
}

public function menu_edit($menu_id){

  $data=array();
$data['menu_edit']=$this->Admin_login_model->menu_edit($menu_id);
$data['show_category']=$this->Admin_login_model->show_post_category();

$data['main_content'] = $this->load->view('admin/edit_menu',$data,true);
$this->load->view('admin/mastering',$data);


}
public function menu_edit_save(){

  $data=array();
    $data['menu_id']=$this->input->post('menu_id',true);
    $data['cat_id']=$this->input->post('cat_id',true);
    $data['sub_cat_id']=$this->input->post('sub_cat_id',true);
    $data['position']=$this->input->post('position',true);
    $status=$this->input->post('menu_status',true);
      if($status==1){
         $data['menu_status']= $status;
      }
      else{
         $data['menu_status']= 0;
      }
$data['update_post']=$this->Admin_login_model->update_menu($data);
  redirect('Admin/menu_list_page');
}



public function add_user(){
$data=array();

$data['user_rol']=$this->Admin_login_model->user_rol();
$data['main_content'] = $this->load->view('admin/add_user',$data,true);
$this->load->view('admin/mastering',$data);

}
public function save_user(){
$data=array();
    $data['username']=$this->input->post('username',true);
    $email=$data['email']=$this->input->post('email',true);
    $data['password']= md5($this->input->post('password',true));
    $data['type']=$this->input->post('type',true);
    $data['status']=$this->input->post('status',true);
    $this->Admin_login_model->insert_user($data,$email);
 redirect('Admin/add_user');

}


function view_user_list(){
  $data=array();
$data['view_user_list']=$this->Admin_login_model->view_user();
$data['main_content'] = $this->load->view('admin/user_list',$data,true);
$this->load->view('admin/mastering',$data);
  }



public function active_user_id($user_id){ 
  $data=array();
  $data['user_id']=$user_id;
  $data['status']=1;
  $this->db->where('user_id',$data['user_id']);
  $this->db->update('tbl_user',$data);
redirect('admin/view_user_list');
}
public function inactive_user_id($user_id){ 
  $data=array();
  $data['user_id']=$user_id;
  $data['status']=0;
  $this->db->where('user_id',$data['user_id']);
  $this->db->update('tbl_user',$data);
redirect('admin/view_user_list');
}

function edit_main_user($user_id){

 $data=array();
$data['edit_main_user']=$this->Admin_login_model->edit_main_user($user_id);
$data['user_rol']=$this->Admin_login_model->user_rol();
// echo '<pre>';
// var_dump($data);die();
$data['main_content'] = $this->load->view('admin/edit_main_user',$data,true);
$this->load->view('admin/mastering',$data);
  
}

function update_main_user($user_id){
    $data=array();
  $password = $this->input->post('password',true);
  $confirm_password =$this->input->post('confirm_password',true);
   if($password && $confirm_password !=''){
   if($password == $confirm_password){
    $data['password']= md5($this->input->post('password',true));

    $data['user_id']=$this->input->post('user_id',true);
    $data['username']=$this->input->post('username',true);
    $data['email']=$this->input->post('email',true);
    // $data['password']= md5($this->input->post('password',true));
    $data['type']=$this->input->post('type',true);
        $status=$this->input->post('status',true);
      if($status==1){
         $data['status']= $status;
      }
      else{
         $data['status']= 0;
      }


  $this->Admin_login_model->update_main_user($data);
       
          $data['success']="Your Data Successfully Update";
          $this->session->set_userdata($data);
          redirect('admin/view_user_list');  

   }

   else{
   $data=array();
   $data['edit_main_user']=$this->Admin_login_model->edit_main_user($user_id);
    $data['error_message']="Your Password dosen't Match";
     $this->session->set_userdata($data);
     redirect('admin/edit_main_user/'.$user_id);  
     
   }
}else{
  $data=array();
      $data['password']= $this->input->post('old_password',true);

     $this->Admin_login_model->update_main_user($data);
      $data['success']="Your Data Successfully Update";
      $this->session->set_userdata($data);
  redirect('admin/view_user_list');

}
 
     
   

}

function delete_main_user($user_id){

   $data=array();
  $data['user_id']=$user_id;
  $this->db->where('user_id',$data['user_id']);
  $this->db->delete('tbl_user',$data);
   redirect('admin/view_user_list');
}



function home_view_settings(){

    $data=array();
    $data['show_category']=$this->Admin_login_model->show_post_category();
    $data['show_home_page_data']=$this->Admin_login_model->show_home_page();
    $data['main_content'] = $this->load->view('admin/home_page_settings',$data,true);
    $this->load->view('admin/mastering',$data);
}

function home_category_position(){

    $data=array();
    $data['home_cat_position']=$this->input->post('home_cat_position',true);
    $data['cat_id']=$this->input->post('category_name',true);
    $data['max_post']=$this->input->post('max_news',true);
   $this->Admin_login_model->insert_home_cat_position($data);


redirect('admin/home_view_settings');

}

function update_home_page(){

    $data=array();
    $home_id= $this->input->post('home_id',true);
    $home_cat_position = $this->input->post('home_cat_position',true);
    $cat_id = $this->input->post('category_name',true);
    $max_post= $this->input->post('max_news',true);
  
        $i=0;
        for($i=0; $i<=count($home_id); $i++){
            $dataHome = array(
              'home_cat_position' =>$home_cat_position[$i],
              'cat_id' =>$cat_id[$i],
              'max_post' =>$max_post[$i]
            );

          $this->db->where('home_id',$home_id[$i])->update('tbl_home',$dataHome);
         
          
 

         } 

          redirect('admin/home_view_settings');
  }




}
