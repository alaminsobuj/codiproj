<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Welcome extends CI_Controller {
        public function __construct() {
        parent:: __construct();
       
        $this->load->library("pagination");
    }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
        $data=array();
  
        $data['show_home_page_fornt']=$this->Admin_login_model->show_home_page_fornt();
        $data['fornt_home2']=$this->Admin_login_model->fornt_home_post1();
        $data['page_fornt']=$this->Admin_login_model->page_fornt();
        $data['show_home_page_fornt2']=$this->Admin_login_model->show_home_page_fornt2();

      



        // $p = array();
        // $i =0;
        // foreach ($show_home_page_fornt as $value) {
            
        //     $p['news_id_'.$i] = $value->post_id;
        //     $p['news_title_'.$i] = $value->title;
        //     $p['news_details_'.$i] = $value->short_description;
        //     $p['news_date_'.$i] = $value->date;
        //     $p['news_post_image_'.$i] = $value->post_image;
        //     $p['news_home_id_'.$i] = $value->home_id;
        //     $p['news_home_cat_position_'.$i] = $value->home_cat_position;
        //     $p['news_cat_id_'.$i] = $value->cat_id;
        //     $p['news_max_post_'.$i] = $value->max_post;
        //     $i++;
        // }
        // $data['hm'] = $p;
// echo "<pre>";
// var_dump($data);
      



		$data['show_post_list']=$this->Admin_login_model->show_post_list($data);

		$data['show_category']=$this->Admin_login_model->show_category_front();
		$data['sub_category']=$this->Admin_login_model->subcategory_front();
        $data['main_content'] = $this->load->view('frond_part/homeTemplate',$data,true);
        $this->load->view('frond_part/mastering',$data);
	}

	function cateory($cat_id,$page=false){
		$config=array();
		$total=$this->Admin_login_model->Post_pagination($cat_id);
        #pagination starts
        #
        $config["base_url"] = base_url('Welcome/cateory/'.$cat_id);
        $config["total_rows"] = $total->total;	
        $config["per_page"] = 2;
        $config["uri_segment"] = 4;
        $config["num_links"] = 5; 
        /* This Application Must Be Used With BootStrap 3 * */
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = "</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tag_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
        /* ends of bootstrap */

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $config['links'] = $this->pagination->create_links();

// end pagination/
       $config['show_post_list']=$this->Admin_login_model->category_page($cat_id,$config['per_page'],$page);

		$config['show_category']=$this->Admin_login_model->show_category_front();
		$config['sub_category']=$this->Admin_login_model->subcategory_front();
		$config['main_content'] = $this->load->view('frond_part/category',$config,true);
        $this->load->view('frond_part/mastering',$config);
	}	

	function post_details($post_id){

        $data=array();
  
		$data['category_details_page']=$this->Admin_login_model->category_details_page($post_id);
		$data['show_category']=$this->Admin_login_model->show_category_front();
		$data['sub_category']=$this->Admin_login_model->subcategory_front();
		$data['main_content'] = $this->load->view('frond_part/details',$data,true);
        $this->load->view('frond_part/mastering',$data);
	}

	
}
